import { nextTick } from 'vue';
import { createRouter, createWebHistory } from 'vue-router';

import HomeView from '@/views/Home';
import AuthView, { LoginView, SignupView } from '@/views/Auth';
import AppView, {
  HomeView as AppHomeView,
  PlansView,
  ProfileView,
} from '@/views/App';

import PlannerView, { WizardView } from '@/views/Planner';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: '',
      component: HomeView,
    },
    {
      path: '/auth',
      name: 'auth',
      component: AuthView,
      children: [
        {
          path: 'login',
          name: 'Log in',
          component: LoginView,
        },
        {
          path: 'signup',
          name: 'Sign up',
          component: SignupView,
        },
      ],
    },
    {
      path: '/app',
      name: 'App',
      component: AppView,
      children: [
        {
          path: 'home',
          name: 'Home',
          component: AppHomeView,
        },
        {
          path: 'my-plans',
          name: 'My plans',
          component: PlansView,
        },
        {
          path: 'profile',
          name: 'Profile',
          component: ProfileView,
        },
        {
          path: 'new-plan',
          name: 'New travel plan',
          component: WizardView,
        },
        {
          path: 'planner/:id',
          name: 'Travel plan',
          component: PlannerView,
        },
        {
          path: 'edit-plan/:id',
          name: 'Edit travel plan',
          component: WizardView,
        },
      ],
    },
  ],
});

router.afterEach((to) => {
  const title =
    'Travel Planner' + (to.name === 'Home' ? '' : ` - ${to.name?.toString()}`);
  nextTick(() => {
    document.title = title;
  });
});

export default router;
