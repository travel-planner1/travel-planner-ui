import AppView from './AppView.vue';

import HomeView from './HomeView.vue';
import PlansView from './PlansView.vue';
import ProfileView from './ProfileView.vue';

export { HomeView, PlansView, ProfileView };

export default AppView;
