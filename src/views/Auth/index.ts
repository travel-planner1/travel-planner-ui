import AuthView from './AuthView.vue';
import LoginView from './LoginView.vue';
import SignupView from './SignupView.vue';

export { LoginView, SignupView };

export default AuthView;
