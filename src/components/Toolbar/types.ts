export interface ToolbarItem {
  id: string;
  label: string;
  icon: string;
  disabled?: boolean;
}
